from random import shuffle

name = raw_input("Unesi ime: ")
print("\nDobrodosli u kviz " + name + "\n")

red = open("questions.txt")
redovi = red.readlines()
shuffle(redovi)
brt = 0
points = open('bodovi.txt','a+')
highScore = points.read()
highScore_in_num = int(0)
for red in redovi:
    question, rightAnswer = red.strip().split("-")
    answer = raw_input(question + ' ')
    if answer.lower() == rightAnswer:
        print('Bravo!\n')
        brt += 1
    else:
        print('Odgovor nije tocan\n')
if brt > highScore_in_num:
    highScore_in_num = brt
    points.write(str(name) + ' ' + str(brt) + "\n")
points.close()

print('Ovo je tablica igraca s bodovima: \n')

poredak = open('bodovi.txt','r')
por = poredak.readlines()
for poredak in por:
    player, score = poredak.strip().split(" ")
    print('Igrac: ' + player + ' ' + 'Bodovi: ' + score)