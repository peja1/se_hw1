# se_hw1

AUTOR: Domagoj Pejakušić

Upute za kviz: 
Ovaj kviz se sastoji od 11 pitanja. Prvo igrač unese svoje ime, zatim dobiva
pitanja koji idu nasumičnim redoslijedom. Igrač za svako pitanje dobije
ponuđen a, b ili c odgovor. Kviz je općeg znanja.

Kako dodati pitanja: Prvo napišemo pitanje, zatim dodamo tri ponuđena
odgovora npr: a) bijelo b) crno c) crveno ?
i onda nakon upitnika BEZ RAZMAKA stavimo crticu i koji je točan odgovor. 
npr: a) bijelo b) crno c) crveno ?-c
Pitanja se dodaju u questions.txt datoteci.
